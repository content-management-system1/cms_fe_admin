import { BrowserRouter as Router,Routes, Route } from 'react-router-dom';
import AddMedia from './media/AddMedia';
import MediaList from './media/MediaList';
import Menu from './Menu';
import SideMenu from './SideMenu';
import EnquiriesRequest from './request/EnquiriesRequest';
import AddUser from './user/AddUser';
import Home from './user/Home';
import UserList from './user/UserList';
import Login from './Login';
import PublisherHome from './publisher/PublisherHome';
import { useState } from "react";
import CreatePost from './PostContent/CreatePost';
import './App.css';
import ShowMedia from './media/ShowMedia';
import MainPost from './PostContent/MainPost';
import ViewPost from './PostContent/ViewPost';

function App() {
  let [loggedin, setLoggedin] = useState(false);
  let [role, setRole] = useState('PUBLISHER');

  if (!loggedin) {
    return <Login doAuth={ setLoggedin } doRole={  setRole }/>
  }

    return (  
        <Router>
          <SideMenu logout= { setLoggedin } role= { role }/>
            <Routes>
              <Route path='/login' element={<Login />}></Route>


              { role === 'ADMIN' && (
                <>
                  <Route path='/' element={<Home />}></Route>
                  <Route path='/user-list' element={<UserList />}></Route>
                  <Route path='/add-user' element={<AddUser />}></Route>
                  <Route path='/media-list' element={<MediaList />}></Route>
                  <Route path='/add-media' element={<AddMedia />}></Route>
                  <Route path='/show-media' element={<ShowMedia />}></Route>
                  <Route path='/request' element={<EnquiriesRequest />}></Route>
                </>
              )} 

              { role === 'PUBLISHER' && (
                <>
                  <Route path='/publisher' element={<PublisherHome />}></Route>
                  <Route path='/post-list' element={<ViewPost />}></Route>
                  <Route path='/add-post' element={<MainPost />}></Route>
                </>
              )}

              
            </Routes>
        </Router>
    );
}

export default App;
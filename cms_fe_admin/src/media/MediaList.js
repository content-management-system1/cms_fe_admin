import { useEffect, useState } from "react";
import ShowMedia from "./ShowMedia";


function MediaList() {
    let [media, setMedia] = useState([]);
    let [mediaSearch, setMediaSearch] = useState([]);
    let [mediaId, setMediaId] = useState(1);
    let [showMedia, setShowMedia] = useState(false);
    let [ titleSearch, setTitleSearch ] = useState('');
    let [ showAllMedia, setShowAllMedia ] = useState(true);
    let [ showMediaSearched, setShowMediaSearch ] = useState(false);


    let doSearch = (event) =>{
        event.preventDefault();
        setShowAllMedia(false);
        setShowMediaSearch(true);
        let titleSearch = event.target.elements.searchInput.value;
        setTitleSearch(titleSearch);

        let url = 'http://localhost:8080/media/search?title='+titleSearch;
        let param = { method: 'GET' };
        fetch(url, param).then((data)=> {
            return data.json();
        }).then((json)=> {
            console.log(json);
            setMediaSearch(json);
        }).catch((err) => {
            console.log(err);
        });
       
    };


    let doDelete = (mediaId) =>{
        let url = 'http://localhost:8080/media/delete/' + mediaId;
        let param = { method: 'DELETE' };
        fetch(url, param).then((data) => {
            return data.json();
        }).then((json) => {
            console.log(json);
            allMedia(); // refresh the data
           
        }).catch((err) => {
            console.log(err);
        });
        alert('deleted');
    };


    const doClickTitle = (mediaId) => {
        setMediaId(mediaId);
        setShowMedia(true);
        console.log(mediaId);
    }

    useEffect(()=>{
        allMedia();
        
    }, [])

    function allMedia(){
        let url = 'http://localhost:8080/media/get-all';
        let param = { method: 'GET' };
        fetch(url, param).then((data)=> {
            return data.json();
        }).then((json)=> {
            console.log(json);
            setMedia(json);
        }).catch((err) => {
            console.log(err);
        });

    }

    

    return (  
        <div style={{ textAlign: "center" }}>
            <div className="row">
                <div className="col-md-12">
                <br /><br /><br /><br /><br />
                <h1>Media Details</h1>
                <form onSubmit={doSearch} >
                    <input type="text" placeholder="search Media name here.." 
                    name="searchInput"/>
                    <button type="submit">Search</button>
                </form>
                    <table className="table table-striped table-bordered table-hover" style={{ margin: "auto" }}>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>File Name</th>
                                <th>Added At</th>
                                <th>Delete</th>
                            </tr>
                        </thead>

                        { showAllMedia && 
                            <tbody>
                                { media.map((m, no) => (
                                    <tr key={m.mediaId}>
                                        <td>{ no + 1 }.</td>
                                        <td><a href="##" onClick={()=>doClickTitle(m.mediaId)}>{ m.title }</a></td>
                                        <td>{ m.name }</td>
                                        <td>{ m.addedAt }</td>
                                        <td><button onClick={() => doDelete(m.mediaId)}>Delete</button></td>
                                    </tr>
                                ))}

                            </tbody>
                        
                        }

                        { showMediaSearched && 
                            <tbody>
                                { mediaSearch.map((m, no) => (
                                    <tr key={m.mediaId}>
                                        <td>{ no + 1 }.</td>
                                        <td><a href="##" onClick={()=>doClickTitle(m.mediaId)}>{ m.title }</a></td>
                                        <td>{ m.name }</td>
                                        <td>{ m.addedAt }</td>
                                        <td><button onClick={() => doDelete(m.mediaId)}>Delete</button></td>
                                    </tr>
                                ))}

                            </tbody>
                        
                        }
                        

                    </table>

                    {showMedia && <ShowMedia isShow={showMedia} mediaId={mediaId} key={mediaId}/>}


                    
                
                </div>



            </div>
            
        </div>
    );
}

export default MediaList;
import { useEffect, useState } from "react";
import { Buffer } from 'buffer';
import './Media.css';


function ShowMedia(props) {
    let [isShow, setIsShow] = useState(false);
    let [image, setImage] = useState(null);
    let [mediaId, setMediaId] = useState(1);

    useEffect(() => {
        setIsShow(props.isShow);
        setMediaId(props.mediaId);
        if (mediaId !== null) {
            let url = 'http://localhost:8080/media/get-image/id/' + mediaId;
            let param = { method: 'GET' };
            fetch(url, param).then((data) => {
                return data.arrayBuffer();
            }).then((buffer) => {
                setImage(Buffer.from(buffer));
    
            }).catch((err) => {
                console.log(err);
            });
        }
    }, [mediaId]);

    if(isShow){
        return ( 
            <div id='popup'>
                <h4>Image</h4>

                <div>
                    <img src={`data:image/png;base64,${image ? image.toString('base64') : ''}`} alt="Media" />
                    <button onClick={() => setIsShow(false)}>close</button>
                </div>

            </div>
           
            
                    
        
        

            );
        }
    }
    

export default ShowMedia;
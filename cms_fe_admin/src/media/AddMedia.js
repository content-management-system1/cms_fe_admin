import React, { useState, useRef, useEffect } from 'react';


function AddMedia() {
    const [file, setFile] = useState(null);
    const [media, setMedia] = useState([]);
    const titleRef = useRef(null);

    const doFileChange = event => setFile(event.target.files[0]);


    const doSubmit = event => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('title', titleRef.current.value);
        formData.append('image', file);
        fetch('http://localhost:8080/media/upload', {
            method: 'POST',
            body: formData
        })
            .then(response => response.json())
            .then(data => {
                setMedia([...media, data]);
                titleRef.current.value = '';
                setFile(null);
            })
            .catch(error => console.log(error));
        alert('successfully uploaded');
    };

    return (  
        <div className="card">
            <div className="card-body text-center">
                <div className="row align-center" style={{justifyContent: 'center'}}>
                    <div className="col-md-6 mx-auto">
                        <h1>Add Media</h1>
                        <p>Enter Media's title below:</p>
                        <form onSubmit={doSubmit}>
                            <div>
                                <label htmlFor="title">Title:</label>
                                <input type="text" id="title" ref={titleRef} />
                            </div>
                            <div>
                                <label htmlFor="file">File:</label>
                                <input type="file" id="file" onChange={doFileChange} />
                            </div>
                            <button type="submit">Upload</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        

    );   
}

export default AddMedia;
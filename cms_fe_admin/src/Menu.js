import { Link } from "react-router-dom";
import { useState } from "react";
import { FiLogOut } from "react-icons/fi";

function Menu({ logout, role }) {

    const doLogout = () =>{
        logout(false);
    }

    const [showDropdown, setShowDropdown] = useState(false);
    const [showDropdown2, setShowDropdown2] = useState(false);

    const toggleDropdown = () => {
    setShowDropdown(!showDropdown);
    };

    const toggleDropdown2 = () => {
        setShowDropdown2(!showDropdown2);
        };

    return (
        <div className="navbar-wrapper d-flex justify-content-center">
            <div className="navbar">
            <h1 style={{ textAlign: "center" }}>WordPush - Editor</h1>
                <div className="links">

                    { role === 'PUBLISHER' && (
                        <>
                            <div className="dropdown">
                                <button className="dropbtn" onClick={toggleDropdown}>
                                    Content Management
                                </button>
                                
                                {showDropdown && (
                                    <div className="dropdown-content">
                                    <Link to="/post-list">View All Posts</Link>
                                    <Link to="/add-post">Create Posts</Link>
                                    </div>
                                )}
                            </div>
                        </>
                    )}

                    { role ==='ADMIN' && (
                        <>
                            <Link to="/">Home</Link>
                            <div className="dropdown">
                                <button className="dropbtn" onClick={toggleDropdown}>
                                    User Management
                                </button>
                                
                                {showDropdown && (
                                    <div className="dropdown-content">
                                    <Link to="/user-list">View All User</Link>
                                    <Link to="/add-user">Add User</Link>
                                    </div>
                                )}
                            </div>

                            <Link to="/request">Enquiries Request</Link>

                            <div className="dropdown">
                                <button className="dropbtn" onClick={toggleDropdown2}>
                                    Media Management
                                </button>
                                {showDropdown2 && (
                                    <div className="dropdown-content">
                                    <Link to="/media-list">View All Media</Link>
                                    <Link to="/add-media">Add Media</Link>
                                    </div>
                                )}
                            </div>
                            
                        </>
                    )}

                    <a href="##" onClick={ doLogout }>Logout<FiLogOut /></a>
                    
                </div>
            </div>         
        </div>
    );

}

export default Menu;
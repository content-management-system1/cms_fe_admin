import React, { useState } from "react";
import DatePicker from "react-datepicker";

function DateDate() {
  const [date, setDate] = useState(new Date());
  function handleDateChange(value) {
    setDate(value);
  }
  return (
    <div className="calendar-input" style={{ position: "relative" }}>
      <DatePicker
        selected={date}
        onChange={handleDateChange}
        showTimeSelect
        dateFormat="Pp"
      />
    </div>
  );
}

export default DateDate;

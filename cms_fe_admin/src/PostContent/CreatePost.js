import { useState} from "react";
import React from "react";
import "react-datepicker/dist/react-datepicker.css";
import ReactQuill, { QuillToolbar } from "react-quill";
import "react-quill/dist/quill.snow.css";
import MediaListPublisher from "./MediaListPublisher";

const Category = Object.freeze({
  HOME: "HOME",
  AboutUs: "ABOUT_US",
  ContactUs: "CONTACT",
});

function CreatePost() {
  // state variables for title, body and image preview
  const [title, setTitle] = useState("");
  // const [body, setBody] = useState(RichTextEditor.createEmptyValue());
  const [body, setBody] = useState("");
  const [imagePreview, setImagePreview] = useState(null);
  // state variable for the selected category
  const [category, setCategory] = useState(Category.Home);
  // state variable for the selected date and time
  const [date, setDate] = useState(new Date());
  // state variable for the selected visibility option
  const [visibility, setVisibility] = useState("public");
  const [isChanged, setIsChanged] = useState(false);
  // state variable for
  const [mediaFile, setMediaFile] = useState(0);
  // state variable for userId
  const [userId, setUserId] = useState(2);
  //try description
  const [description, setDescription] = useState("");

  // handle change events for title fields
  function handleTitleChange(e) {
    setTitle(e.target.value);
  }
  // handle change events for body fields
  function handleBodyChange(content, delta, source, editor) {
    const plainText = editor.getText();
    setBody(plainText);
  }

  //set mediaId from props
  function updateMediaId(id) {
    setMediaFile(id);
    console.log(mediaFile);
  }
  // handle timer to check
  // function handleTimePass() {
  //   console.log("yes");
  //   if (isChanged === true) {
  //     //have to create/update file
  //     setIsChanged(false);
  //   }
  // }

  //handle change events for category drop down fields.
  function handleCategoryChange(e) {
    setCategory(e.target.value);
  }

  //handle change events for date picker field.
  function handleDateChange(value) {
    setDate(value);
  }

  // useEffect(() => {
  //   const intervalId = setInterval(() => {
  //     handleTimePass();
  //   }, 5000);
  // }, []);

  // handle form submission
  function handleSubmit(e) {
    e.preventDefault();
    let url = "http://localhost:8080/contents/create/" + mediaFile;
    let data = {
      title: title,
      description: body,
      category: category,
      userId: { userId: userId },
    };
    let some = JSON.stringify(data);
    console.log(some);
    console.log(title);
    console.log(body);
    console.log(category);
    console.log(userId);
    let param = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    };
    fetch(url, param)
      .then((data) => data.json())
      .then((json) => {
        console.log(json);
        alert("Content was successfully posted");
      });
  }

  return (
    <div>
      <div >
        <form onSubmit={handleSubmit}>
          <label htmlFor="title">Title</label>
          <input
            type="text"
            id="title"
            value={title}
            onChange={handleTitleChange}
          />

          <button type="submit" className="" onClick={handleSubmit}>
            Publish
          </button>
          <label htmlFor="body">Body</label>
          <div dir="ltr">
          <ReactQuill
            value={body}
            onChange={handleBodyChange}
            modules={{ toolbar: QuillToolbar }}
            writingDirection="ltr"
            rtl={false}
          /></div>

          <label htmlFor="category">Category</label>
          <select
            id="category"
            value={category}
            onChange={handleCategoryChange}
          >
            {Object.values(Category).map((value) => (
              <option key={value} value={value}>
                {value}
              </option>
            ))}
          </select>

          <br />
        </form>
        <MediaListPublisher updateMediaId={updateMediaId} />
      </div>
    </div>
  );
}
export default CreatePost;

import { useEffect, useState } from "react";
import PostDetails from "./PostDetails";

function ViewPost() {
  let [post, setPost] = useState([]);
  let [contentId, setContentId] = useState(1);

  useEffect(() => {
    console.log("PostDetails useEffect called");
    let url = "http://localhost:8080/contents/all";
    let param = { method: "GET" };
    fetch(url, param)
      .then((data) => {
        return data.json();
      })
      .then((json) => {
        console.log(json);
        setPost(json);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

 
  const doClick = (contentId) => {
    setContentId(contentId);
    console.log(contentId);
  };

  let doDelete = (contentId) =>{
    let url = 'http://localhost:8080/contents/' + contentId;
    let param = { method: 'DELETE' };
    fetch(url, param).then((data) => {
        return data.json();
    }).then((json) => {
        console.log(json);
        allPosts();
        // window.history.pushState({}, null, window.location.pathname);
       
    }).catch((err) => {
        console.log(err);
    });
    alert('deleted');
};

function allPosts(){
    let url = 'http://localhost:8080/contents/all';
    let param = { method: 'GET' };
    fetch(url, param).then((data)=> {
        return data.json();
    }).then((json)=> {
        console.log(json);
        setPost(json);
    }).catch((err) => {
        console.log(err);
    });

}
  return (
    <div>
      <div className="form-container">
        <div className="col-md-8" id="search-list">
          <h1>List</h1>
          <table className="table table-bordered table-striped">
            <thead>
              <tr>
                <th>NO</th>
                <th>TITLE</th>
                <th>DESCRIPTION</th>
                <th>IMAGE</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {post.map((p, no) => (
                <tr key={p.contentId}>
                  <td>{no + 1}.</td>
                  <td>
                    <a href="##" onClick={() => doClick(p.contentId)}>
                      {p.title}
                    </a>
                  </td>
                  <td>{p.description}</td>
                  <td>
                    <img
                      src={`data:image/png;base64,${
                        p.image ? p.image.toString("base64") : ""
                      }`}
                    />
                  </td>
                  <td><button onClick={() => doDelete(p.contentId)}>Remove</button></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        
      </div>
      <div className="col-md-4"><PostDetails contentId={contentId} title={contentId.title}/></div>
    </div>
  );
}

export default ViewPost;

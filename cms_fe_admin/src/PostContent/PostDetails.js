import { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { CardActionArea } from '@mui/material';

function PostDetails(props, postId) {
  const [postDetails, setPostDetails] = useState({});

  console.log("postId received in PostDetails:", props.contentId);
  

  
  useEffect(() => {
    const {contentId} = props;
    let url = `http://localhost:8080/contents/${postId}`;
    let param = { method: "GET" };
    fetch(url, param)
      .then((data) => {
        return data.json();
      })
      .then((json) => {
        console.log(json);
        setPostDetails(json);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [props.postId]);

  return (
    <Card sx={{ maxWidth: 345 }}>
        <CardActionArea>
      <CardContent>
        {/* <div>
          <h1>Post</h1>
          <div>Title : {props.title}</div>
          <br />
          <div>{postDetails.description}</div>
          <br /> */}
          {/* <ul>
            {postDetails.map((actor) => (
              <li key={actor.actorId}>
                {actor.firstName} {actor.lastName}
              </li>
            ))}
          </ul> */}
        {/* </div> */}
      </CardContent>
      </CardActionArea>
    </Card>
  );
}

export default PostDetails;

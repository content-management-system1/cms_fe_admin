import { useEffect, useState } from "react";

function EnquiriesRequest() {
    let [ request, setRequest ] = useState([]);


    let doDelete = (requestId) =>{
        let url = 'http://localhost:8080/request/' + requestId;
        let param = { method: 'DELETE' };
        fetch(url, param).then((data) => {
            return data.json();
        }).then((json) => {
            console.log(json);
            allRequest(); // refresh the data
           
        }).catch((err) => {
            console.log(err);
        });
        alert('Deleted');
    };

    

    useEffect(()=>{
        allRequest();
        
    }, [])

    function allRequest(){
        let url = 'http://localhost:8080/all-request';
        let param = { method: 'GET' };
        fetch(url, param).then((data)=> {
            return data.json();
        }).then((json)=> {
            console.log(json);
            setRequest(json);
        }).catch((err) => {
            console.log(err);
        });

    }

    return (  
        <div style={{ textAlign: "center" }}>
            <div className="row">
                <div className="col-md-12">
                <br /><br /><br /><br /><br />
                <h1>Enquiries Requests</h1>
                    <table className="table table-striped table-bordered table-hover" style={{ margin: "auto" }}>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Query</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            { request.map((r, no) => (
                                <tr key={r.requestId}>
                                    <td>{ no + 1 }.</td>
                                    <td>{ r.name }</td>
                                    <td>{ r.email }</td>
                                    <td>{ r.phoneNumber }</td>
                                    <td>{ r.query }</td>
                                    <td><button onClick={() => doDelete(r.requestId)}>Delete</button></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>



            </div>
            
        </div>
    );
}

export default EnquiriesRequest;
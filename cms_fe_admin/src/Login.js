import { useRef, useState } from "react";
import Button from '@mui/material/Button';
import './index.css';


// instead of using props, we try another method to pass the do log in value
//we destructure it
function Login({ doAuth, doRole }) {
    let username = useRef();
    let password = useRef();
    let [msg, setMsg] = useState();

    function doLogin(event) {
        event.preventDefault(); 
        //stop default behavior
        console.log(username.current.value, password.current.value);

        const enteredUsername = username.current.value;
        const enteredPassword = password.current.value;

        fetch('http://localhost:8080/user/login', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            username: enteredUsername,
            password: enteredPassword
            })
        })
        .then(response => response.json())
        .then(data => {
            // Check if login was successful
            if (data.userId) {
            // User found, allow login
            doAuth(true);
            doRole(data.role); // Pass the user's role to the doRole function
            } else {
            // User not found, display error message
            setMsg('Ops! Wrong username or password!');
            }
        })
        .catch(error => {
            console.error(error);
            setMsg('Ops! An error occurred while logging in.');
        });
    }

    return (  
        <div className="card">
            <div className="card-body text-center">
                <div className="row align-center" style={{justifyContent: 'center'}}>
                    <div className="col-md-6 mx-auto">
                        <div name="login">
                            <h1>Login Page</h1>
                            <br/>
                            { msg && <div className="alert alert-danger">{ msg }</div>}
                            <form onSubmit={ doLogin }>

                                <div className="row">
                                    <div className="col-md-12">
                                        Username
                                        <input type="text" className="form-control" ref={ username }/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12">
                                        Password
                                        <input type="password" className="form-control" ref={ password }/>
                                    </div>
                                </div>

                                
                                <div className="row mt-2">
                                    <div className="col-md-12">
                                        <Button type="submit" variant="contained">Login</Button>
                                        
                                    </div>
                                </div>

                            </form>
                
                        </div>

                    </div>
                </div>
            </div>
        </div>

        
    );
}

export default Login;

import { useState, useEffect } from "react";
import './User.css';


function UserEdit(props) {
    let [isShow, setIsShow] = useState(false);
    let [ username, setUsername ] = useState('');
    let [ userId, setUserId ] = useState('');
    let [ email, setEmail ] = useState('');
    let [ password, setPassword ] = useState('');
    let [ role, setRole ] = useState('PUBLISHER');


    useEffect(() => {
        setIsShow(props.isShow);
        let url = 'http://localhost:8080/user/id/'+props.userId;
        let param = { method: 'GET' };
        fetch(url,param)
        .then(data => data.json())
        .then(json => {
            setUsername(json.username);
            setUserId(json.userId);
            setEmail(json.email);
            setPassword(json.password);
            setRole(json.role);
        });
    }, []);

    function doSave(){
        let url = 'http://localhost:8080/user/edit';
        let data = {userId, username, email, password, role}
        let param = { 
            method: 'POST',
            headers: { 'Content-type': 'application/json'},
            body: JSON.stringify(data)
        };
        fetch(url,param)
        .then(data => data.json())
        .then(json => {
            console.log(json);
            setIsShow(false);
        });
    }
    
    
    if (isShow) {
        return (  
            <div id='myform'>
                <h4>Edit User</h4>
                <div className='row'>
                    <div className='col-md-12'>
                        <label>Username</label>
                        <input type='text' className='form-control' value={username}
                        onChange={(event)=> setUsername(event.target.value)}/>
                    </div>
                </div>

                <div className='row mb-2'>
                    <div className='col-md-12'>
                        <label>Email</label>
                        <textarea className='form-control' value={email}
                        onChange={(event)=> setEmail(event.target.value)}></textarea>
                    </div>
                </div>

                <div className='row mb-2'>
                    <div className='col-md-12'>
                        <label>Password</label>
                        <textarea className='form-control' value={password}
                        onChange={(event)=> setPassword(event.target.value)}></textarea>
                    </div>
                </div>

                <div className="row mb-2">
                    <div className="col-md-2">Role: </div>
                        <div className="col-md-4">
                            <select className="form-control" value={role}
                            onChange={(event)=> setRole(event.target.value)}>
                                <option value="PUBLISHER">Publisher</option>
                                <option value="ADMIN">Admin</option>
                            </select>
                        </div>
                    </div>

                

                <div className='row'>
                    <div className='col-md-12'>
                        <button className='btn btn-primary' onClick={ doSave }>Submit</button>
                        <button onClick={()=>setIsShow(false)} className='btn btn-warning'>Close</button>
                    </div>
                </div>

            </div>
        );
    } else {
        return null;
    }
}

export default UserEdit;
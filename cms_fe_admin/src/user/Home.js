function Home() {
    return (  
        <div className="card">
            <div className="card-body text-center">
                <div className="row align-center" style={{justifyContent: 'center'}}>
                    <div className="col-md-6 mx-auto">
                        <h1>Admin-Home</h1>
                        <div >
                            <br/>
                            <h3>Welcome back!</h3>
                            <p>Remember to start your day by checking any new enquiries request!</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;
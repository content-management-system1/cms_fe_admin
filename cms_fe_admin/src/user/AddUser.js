import './User.css';
import { useState } from 'react';

function AddUser() {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('PUBLISHER'); 


    const doSubmit= (event) => {
        event.preventDefault();
        let url = 'http://localhost:8080/user/create';
        let data = {username, password, email, role}
        let param = { 
            method: 'POST',
            headers: { 'Content-type': 'application/json'},
            body: JSON.stringify(data)
        };
        fetch(url, param)
            .then((response) => {
            if (!response.ok) {
                throw new Error('Username already exists.'); // throw an error if username already exists
            }
            return response.json();
            })
            .then((json) => {
            console.log(json);
            alert('User successfully created');
            })
            .catch((error) => {
            console.error(error);
            alert(error.message); // display the error message in an alert
            });
    }

    return (  
        <div className="card">
            <div className="card-body text-center">
                <div className="row align-center" style={{justifyContent: 'center'}}>
                    <div className="col-md-6 mx-auto">
                        <h1>Add User</h1>
                        <p>Enter user's details below:</p>

                        <form onSubmit={doSubmit}>
                            <div className="row mb-2 align-center">
                                <div className="col-md-2">Username: </div>
                                <div className="col-md-4">
                                    <input type="text" className="form-control" 
                                    placeholder="username" value={username}
                                    onChange={(event)=> setUsername(event.target.value)}/>
                                </div>
                            </div>

                            <div className="row mb-3">
                                <div className="col-md-2">Email: </div>
                                <div className="col-md-4">
                                    <input type="text" className="form-control" 
                                    placeholder="email address" value={email}
                                    onChange={(event)=> setEmail(event.target.value)}/>
                                </div>
                            </div>

                            <div className="row mb-2">
                                <div className="col-md-2">Password: </div>
                                <div className="col-md-4">
                                    <input type="text" className="form-control" 
                                    placeholder="password" value={password}
                                    onChange={(event)=> setPassword(event.target.value)}/>
                                </div>
                            </div>

                            <div className="row mb-2">
                                <div className="col-md-2">Role: </div>
                                <div className="col-md-4">
                                    <select className="form-control" value={role}
                                    onChange={(event)=> setRole(event.target.value)}>
                                        <option value="PUBLISHER">Publisher</option>
                                        <option value="ADMIN">Admin</option>
                                    </select>
                                </div>
                            </div>

                            <button type="submit" className="btn btn-primary">Submit</button>

                        </form>

                        
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AddUser;
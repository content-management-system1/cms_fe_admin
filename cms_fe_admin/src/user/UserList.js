import { useEffect, useState } from "react";
import UserEdit from "./UserEdit";

function UserList() {
    let [ user, setUser ] = useState([]);
    let [ userSearch, setUserSearch ] = useState([]);
    let[showEdit, setShowEdit] = useState(false);
    let [ userId, setUserId ] = useState(1);
    let [ showAllUser, setShowAllUser ] = useState(true);
    let [ showUserSearched, setShowUserSearch ] = useState(false);
    let [ usernameSearch, setUsernameSearch ] = useState('');

    let doSearch = (event) =>{
        event.preventDefault();
        setShowAllUser(false);
        setShowUserSearch(true);
        let usernameSearch = event.target.elements.searchInput.value;
        setUsernameSearch(usernameSearch);

        let url = 'http://localhost:8080/user/search?username='+ usernameSearch;
        let param = { method: 'GET' };
        fetch(url, param).then((data)=> {
            return data.json();
        }).then((json)=> {
            console.log(json);
            setUserSearch(json);
        }).catch((err) => {
            console.log(err);
        });
    };

    let doEdit = (userId) =>{
        setShowEdit(true);
        setUserId(userId);
        
    };

    let doDelete = (userId) =>{
        let url = 'http://localhost:8080/user/delete/' + userId;
        let param = { method: 'DELETE' };
        fetch(url, param).then((data) => {
            return data.json();
        }).then((json) => {
            console.log(json);
            allUser(); // refresh the data
           
        }).catch((err) => {
            console.log(err);
        });
        alert('Deleted');
    };

    useEffect(()=>{
        allUser();
        
    }, [])

    function allUser(){
        let url = 'http://localhost:8080/user/get-all';
        let param = { method: 'GET' };
        fetch(url, param).then((data)=> {
            return data.json();
        }).then((json)=> {
            console.log(json);
            setUser(json);
        }).catch((err) => {
            console.log(err);
        });

    }

    return (  
        <div style={{ textAlign: "center" }}>
            <div className="row">
                <div className="col-md-12">
                <br /><br /><br /><br /><br />
                <h1>User Details</h1>
                <form onSubmit={doSearch}>
                    <input type="text" placeholder="search username here.." 
                    name="searchInput"/>
                    <button type="submit">Search</button>
                </form>
                    <table className="table table-striped table-bordered table-hover" style={{ margin: "auto" }}>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Role</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>

                        { showAllUser && 
                            <tbody>
                                { user.map((u, no) => (
                                    <tr key={u.userId}>
                                        <td>{ no + 1 }.</td>
                                        <td>{ u.username }</td>
                                        <td>{ u.email }</td>
                                        <td>{ u.password }</td>
                                        <td>{ u.role }</td>
                                        <td><button onClick={() => doEdit(u.userId)}>Edit</button></td>
                                        <td><button onClick={() => doDelete(u.userId)}>Delete</button></td>
                                    </tr>
                                ))}

                            </tbody>
                        }

                        { showUserSearched && 
                        <tbody>
                            { userSearch.map((u, no) => (
                                <tr key={u.userId}>
                                    <td>{ no + 1 }.</td>
                                    <td>{ u.username }</td>
                                    <td>{ u.email }</td>
                                    <td>{ u.password }</td>
                                    <td>{ u.role }</td>
                                    <td><button onClick={() => doEdit(u.userId)}>Edit</button></td>
                                    <td><button onClick={() => doDelete(u.userId)}>Delete</button></td>
                                </tr>
                            ))}

                        </tbody>
                        }
                        
                    </table>

                    {showEdit && <UserEdit isShow={showEdit} userId={userId} key={userId}/>}
                </div>



            </div>
            
        </div>
    );
}

export default UserList;
import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import LogoutIcon from '@mui/icons-material/Logout';
import HomeIcon from '@mui/icons-material/Home';
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import GroupIcon from '@mui/icons-material/Group';
import ImageIcon from '@mui/icons-material/Image';
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import { Link } from "react-router-dom";
const drawerWidth = 240;

export default function SideMenu({ logout, role }) {
  const doLogout = () => {
    logout(false);
  };
  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar
        position="fixed"
        sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px` }}
      >
        <Toolbar>
          <Typography variant="h6" noWrap component="div">
            WordPush
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: drawerWidth,
            boxSizing: "border-box",
          },
        }}
        variant="permanent"
        anchor="left"
      >
        <Toolbar />
        <Divider />
        <List> 
          { (role === 'PUBLISHER') && (
            <>
                {["Create Posts", "View Posts"].map((text, index) => (
                <ListItem key={text} disablePadding>
                  <ListItemButton
                    component={Link}
                    to={index === 0 ? "/add-post" : "/post-list"}
                  >
                    <ListItemIcon>
                      {index % 2 === 0 ? <MailIcon /> : <InboxIcon />}
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItemButton>
                </ListItem>
              ))}
            </>

          )}

          { (role === 'ADMIN') && (
            <>
                {["Home", "View All User", "Add User"].map((text, index) => (
                <ListItem key={text} disablePadding>
                  <ListItemButton
                    component={Link}
                    to={index === 0 ? "/" : index === 1 ? "/user-list" : "/add-user"}
                  >
                    <ListItemIcon>
                      {index % 3 === 0 ? <HomeIcon /> : index === 1 ? <GroupIcon /> : <PersonAddAltIcon />}
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItemButton>
                </ListItem>
              ))}
              <Divider />
              {["View All Media", "Add Media"].map((text, index) => (
                <ListItem key={text} disablePadding>
                  <ListItemButton
                    component={Link}
                    to={index === 0 ? "/media-list" : "/add-media"}
                  >
                    <ListItemIcon>
                      {index % 2 === 0 ? <ImageIcon /> : <AddPhotoAlternateIcon />}
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItemButton>
                </ListItem>
              ))}
              <Divider />
              {["Enquiries Request"].map((text, index) => (
                <ListItem key={text} disablePadding>
                  <ListItemButton
                    component={Link}
                    to="/request"
                  >
                    <ListItemIcon>
                      {index === 0 && <QuestionAnswerIcon />}
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItemButton>
                </ListItem>
              ))}
              
              
            </>

          )}
          <br/>
          
          <ListItem key="Logout" disablePadding>
            <ListItemButton onClick={doLogout}>
              <LogoutIcon />
              <ListItemIcon/>
              <ListItemText primary="Logout" />
            </ListItemButton>
          </ListItem>
        </List>
      </Drawer>
    </Box>
  );
}
